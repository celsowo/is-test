require 'rails_helper'

RSpec.describe Classroom, :type => :model do

  it "should have valid factory" do
    expect(FactoryGirl.build(:classroom).valid?).to eq(true)
	end

  it 'should require student' do
  	expect(FactoryGirl.build(:classroom, student: nil).valid?).to eq(false)
  end

  it 'should require description' do
  	expect(FactoryGirl.build(:classroom, course: nil).valid?).to eq(false)
  end

  it 'should auto fill entry_at' do
  	expect(FactoryGirl.create(:classroom).entry_at.nil?).to eq(false)
  end

end
