require 'rails_helper'

RSpec.describe Course, :type => :model do

	it "should have valid factory" do
    expect(FactoryGirl.build(:course).valid?).to eq(true)
	end

  it 'should require name' do
  	expect(FactoryGirl.build(:course, name: '').valid?).to eq(false)
  end

  it 'should require description' do
  	expect(FactoryGirl.build(:course, description: '').valid?).to eq(false)
  end

  it 'should require status' do
  	expect(FactoryGirl.build(:course, status: nil).valid?).to eq(false)
  end

  it 'should destroy related classrooms' do
  	course = FactoryGirl.create(:course)
  	student = FactoryGirl.create(:student)

  	classroom = Classroom.create(course: course, student: student)
  	classroom.save

  	course.destroy
  	expect(Classroom.where(id: classroom.id).count).to eq(0)
  end

end
