require 'rails_helper'

RSpec.describe Student, :type => :model do

  it "should have valid factory" do
    expect(FactoryGirl.build(:student).valid?).to eq(true)
	end

  it 'should require name' do
  	expect(FactoryGirl.build(:student, name: '').valid?).to eq(false)
  end

  it 'should require register_number' do
  	expect(FactoryGirl.build(:student, register_number: '').valid?).to eq(false)
  end

  it 'should require status' do
  	expect(FactoryGirl.build(:student, status: nil).valid?).to eq(false)
  end

  it 'should destroy related classrooms' do
  	course = FactoryGirl.create(:course)
  	student = FactoryGirl.create(:student)

  	classroom = Classroom.create(course: course, student: student)
  	classroom.save

  	student.destroy
  	expect(Classroom.where(id: classroom.id).count).to eq(0)
  end

end
