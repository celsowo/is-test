# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :student do
    name 'Student'
    register_number "123456X"
    status :active
  end
end
