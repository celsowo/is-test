Rails.application.routes.draw do

  root 'dashboard#index'

  get '' => 'dashboard#index'

  resources :classrooms
  resources :courses
  resources :students

end
