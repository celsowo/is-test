class StudentsController < ApplicationController

	def index
		@students = Student.all.order(:name).paginate(page:params[:page], per_page: 10)
	end

	def show
		@student = Student.find(params[:id])
	end

	def new
		@student = Student.new
	end

	def create
		@student = Student.new(permited_params)

		if @student.save
			flash[:notice] = I18n.t('flash.success')
			redirect_to @student
		else
			render 'new'
		end
	end

	def edit
		@student = Student.find(params[:id])
	end

	def update
		@student = Student.find(params[:id])

		if @student.update(permited_params)
			flash[:notice] = I18n.t('flash.success')
			redirect_to @student
		else
			render 'edit'
		end
	end

	def destroy
    @student = Student.find(params[:id])
    @student.destroy
 
 		flash[:notice] = I18n.t('flash.success')
    redirect_to students_path
  end

	private
	def permited_params
		params.require(:student).permit(:name, :register_number, :status)
	end

end
