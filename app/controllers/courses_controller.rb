class CoursesController < ApplicationController

	def index
		@courses = Course.all.order(:name).paginate(page:params[:page], per_page: 10)
	end

	def show
		@course = Course.find(params[:id])
	end

	def new
		@course = Course.new
	end

	def create
		@course = Course.new(permited_params)

		if @course.save
			flash[:notice] = I18n.t('flash.success')
			redirect_to @course
		else
			render 'new'
		end
	end

	def edit
		@course = Course.find(params[:id])
	end

	def update
		@course = Course.find(params[:id])

		if @course.update(permited_params)
			flash[:notice] = I18n.t('flash.success')
			redirect_to @course
		else
			render 'edit'
		end
	end

	def destroy
    @course = Course.find(params[:id])
    @course.destroy
 		
 		flash[:notice] = I18n.t('flash.success')
    redirect_to courses_path
  end

	private
	def permited_params
		params.require(:course).permit(:name, :description, :status)
	end

end
