class ClassroomsController < ApplicationController
	
	def index
		@classrooms = Classroom.all.order(:entry_at).paginate(page:params[:page], per_page: 10)
	end

	def show
		@classroom = Classroom.find(params[:id])
	end

	def new
		@classroom = Classroom.new
	end

	def create
		@classroom = Classroom.new(permited_params)

		if @classroom.save
			flash[:notice] = I18n.t('flash.success')
			redirect_to @classroom
		else
			render 'new'
		end
	end

	def edit
		@classroom = Classroom.find(params[:id])
	end

	def update
		@classroom = Classroom.find(params[:id])

		if @classroom.update(permited_params)
			flash[:notice] = I18n.t('flash.success')
			redirect_to @classroom
		else
			render 'edit'
		end
	end

	def destroy
    @classroom = Classroom.find(params[:id])
    @classroom.destroy
 
 		flash[:notice] = I18n.t('flash.success')
    redirect_to classrooms_path
  end

	private
	def permited_params
		params.require(:classroom).permit(:course_id, :student_id)
	end

end
