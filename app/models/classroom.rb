class Classroom < ActiveRecord::Base
	belongs_to :course
	belongs_to :student

	validates_presence_of :course_id, :student_id
	validates_uniqueness_of :student_id, scope: :course_id, 
													message: I18n.t('activerecord.validations.classroom.uniqueness')

	before_create do
		self.entry_at = Time.now
	end
end
