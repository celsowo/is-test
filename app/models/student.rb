class Student < ActiveRecord::Base
	enum status: [ :active, :inactive ]

	has_many :classrooms, dependent: :destroy

	validates_presence_of :name, :register_number, :status

end
