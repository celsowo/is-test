# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# The seed below was extract from Alura website
# $('li.product').each(function(index,object){
# 	var title = $(object).find('h3').html();
# 	var description = $(object).find('p').html();
# 	console.log("Course.create({name: '" + title + "', description: '" + description + "', status: :active})");
# });

Course.create({name: 'PHP', description: 'Pronto para criar aplicações web de maneira produtiva?', status: :active})
Course.create({name: 'C# e .NET', description: 'C# está cada dia mais popular. Domine a plataforma da Microsoft', status: :active})
Course.create({name: 'Java para Web', description: 'Desenvolva na Web com todo poder do Java', status: :active})
Course.create({name: 'HTML e Front End', description: 'Trabalhe o front-end com modernas técnicas de HTML, CSS e JavaScript', status: :active})
Course.create({name: 'Infraestrutura', description: 'Crie e mantenha o ambiente das suas aplicações', status: :active})
Course.create({name: 'iOS', description: 'Aprenda a desenvolver para os celulares e tablets mais populares do mundo.', status: :active})
Course.create({name: 'C# e .NET para Web', description: 'ASP.NET MVC, Razor, Entity Framework e tudo que você precisa saber para desenvolver para web!', status: :active})
Course.create({name: 'Computação', description: 'Estude as materias da sua faculdade.', status: :active})
Course.create({name: 'Python', description: 'Programe e crie aplicações com a lendária linguagem', status: :active})
Course.create({name: 'Java', description: 'Comece aqui a dominar a linguagem mais famosa do planeta!', status: :active})
Course.create({name: 'Agilidade', description: 'Faça sua equipe ser produtiva e entregar valor de uma vez por todas.', status: :active})
Course.create({name: 'Java Avançado', description: 'Está na hora de se tornar um mestre na linguagem.', status: :active})
Course.create({name: 'Certificação Java', description: 'Prepare-se já para a prova de certificação da Oracle!', status: :active})
Course.create({name: 'Ruby e Rails', description: 'Pronto para desenvolver de maneira produtiva?', status: :active})
Course.create({name: 'Android', description: 'Os celulares estão por toda a parte. Aprenda Android e crie aplicações para milhões de pessoas.', status: :active})
Course.create({name: 'Primeiros Passos', description: 'Desenvolvedor de software é a profissão do futuro. Comece agora mesmo!', status: :active})
Course.create({name: 'Testes de Software', description: 'Projeto de qualidade tem testes, do começo ao fim.', status: :active})
Course.create({name: 'Design e UX', description: 'Solte seu lado criativo, e aprenda design para web', status: :active})

# Extract from http://www.facha.edu.br/lista-de-aprovados-enem
# jQuery('.tz-article').find('tr').each(function(index, object){
# 	var register = jQuery(jQuery(object).children('td')[0]).html();
# 	var name = jQuery(jQuery(object).children('td')[1]).html();
# 	console.log("Student.create({name: '" + name + "', register_number: '" + register + "', status: :active})");
# });

Student.create({name: 'HANNA CAROLINA CARNEIRO DE OLIVEIRA', register_number: '123928', status: :active})
Student.create({name: 'ISABELLA VERONEZ DE SOUZA', register_number: '123991', status: :active})
Student.create({name: 'LAÍS SILVA VIEIRA', register_number: '123985', status: :active})
Student.create({name: 'LETÍCIA PINTO FIGUEIREDO', register_number: '123992', status: :active})
Student.create({name: 'SERGIO RODRIGUES DA SILVA', register_number: '123932', status: :active})
Student.create({name: 'THAYS CRISTINA DE VASCONCELOS FIUZA', register_number: '123957', status: :active})
Student.create({name: 'VITOR PEREIRA DA COSTA SANTOS', register_number: '123986', status: :active})
Student.create({name: 'ATON APARICIO DE OLIVEIRA', register_number: '124023', status: :active})
Student.create({name: 'LETICIA DE BARROS MORETT', register_number: '124044', status: :active})
Student.create({name: 'MANOELLA GOMES DELUNARDO SEVERINO', register_number: '124047', status: :active})
Student.create({name: 'MARIA LUIZA DUGIN MONNERAT DE AZEVEDO', register_number: '124024', status: :active})
Student.create({name: 'MILENA DA SILVA SACRAMENTO', register_number: '124012', status: :active})
Student.create({name: 'CAMILA CERQUEIRA PIRES', register_number: '124115', status: :active})
Student.create({name: 'LIDIANE DE FREITAS PEREIRA', register_number: '124117', status: :active})
Student.create({name: 'TÁRIK GONÇALVES EL ZEIN', register_number: '124015', status: :active})
